<?php
    header('Content-Type: text/html; charset=utf-8');
    header('Access-Control-Allow-Origin: *');
    header('Access-Control-Allow-Methods: POST, GET');
    header('Access-Control-Allow-Headers: Content-Type');

    $server = "localhost";
    $sqlid = ""; // username for MySQL database provided during installation
    $sqlpass = ""; // password for MySQL database provided during installation
    $dbase = ""; // name of MySQL database
    $mysqli = new mysqli($server, $sqlid, $sqlpass, $dbase);

    if (mysqli_connect_errno()) {
        // echo "ERROR: <br>Failed to connect to MySQL: " . mysqli_connect_error();
        echo "Unfortunately, something broke! Please submit an issue <a href='https://gitlab.com/spechide/QuizGenerator/issues'>here</a>! :( ";
    }

    // change the below options if your table name in the MySQL database is differnet
    $QuestionsTableName = "QuizGeneratorMoin";
    $UsersTableName = "QuizGeneratorUsers";

    date_default_timezone_set('Asia/Calcutta'); // change this if your timezone is different
    setlocale(LC_MONETARY, 'en_IN'); // change this if you live in a different country

    $requiredquestions = 10;
?>
