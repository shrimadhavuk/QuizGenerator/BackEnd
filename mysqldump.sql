-- phpMyAdmin SQL Dump
-- version 4.7.0-dev
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 24, 2016 at 02:23 AM
-- Server version: 10.1.14-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ICFOSS`
--

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `QuizGeneratorMoin` (
  `qno` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `opta` varchar(500) NOT NULL,
  `optb` varchar(500) NOT NULL,
  `optc` varchar(500) NOT NULL,
  `optd` varchar(500) NOT NULL,
  `cans` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `QuizGeneratorUsers` (
  `regno` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `qno` int(11) NOT NULL,
  `mans` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `questions`
--
ALTER TABLE `QuizGeneratorMoin`
  ADD PRIMARY KEY (`qno`);

--
-- Indexes for table `users`
--
ALTER TABLE `QuizGeneratorUsers` ADD PRIMARY KEY( `regno`, `qno`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
