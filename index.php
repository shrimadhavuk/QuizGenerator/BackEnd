<?php
require_once __DIR__ . "/config.php";

$request = trim($_SERVER['PATH_INFO'], '/');
$returnarray = array();
header('Content-Type: application/json');

if ($request == "readquestions") {
    require_once __DIR__ . "/_ReadQuestions.php";
} elseif ($request == "updateanswers") {
    require_once __DIR__ . "/_UpdateAnswers.php";
} elseif ($request == "readscore") {
    require_once __DIR__ . "/_ReadScore.php";
} elseif ($request == "createquestion") {
    require_once __DIR__ . "/_CreateQuestion.php";
} elseif ($request == "readusers") {
    $returnarray["message"] = "Under Construction!";
} elseif ($request == "totalquestions") {
    $returnarray["totalquestions"] = $totalquestions;
} else {
    require_once __DIR__ . "/_404.php";
}

echo json_encode($returnarray);
?>
