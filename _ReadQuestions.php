<?php
$query = "SELECT * FROM `$QuestionsTableName`;";
$result = $mysqli->query($query);
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $temporaryarray = array();
        $temporaryarray["QNO"] = $row['qno'];
        $temporaryarray["QUESTION"] = utf8_encode($row['question']);
        $temporaryarray["ANS_A"] = utf8_encode($row['opta']);
        $temporaryarray["ANS_B"] = utf8_encode($row['optb']);
        $temporaryarray["ANS_C"] = utf8_encode($row['optc']);
        $temporaryarray["ANS_D"] = utf8_encode($row['optd']);
        $returnarray[] = $temporaryarray;
    }
} else {
    require_once __DIR__ . "/_403.php";
}
?>
